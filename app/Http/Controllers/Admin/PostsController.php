<?php

    namespace App\Http\Controllers\Admin;

    /**
     * VoIP Technology S.A.
     * Control De Blog.
     *
     * @copyright Copyright (c) 2018, VoIP Technology S.A.
     * @link   http://voiptech.com.mx
     * @author Ramses Aguirre Farrera <ramsesaguirre2012@gmail.com>
     * @author Contacto <contacto#voiptech.com.mx>
     * @license Contrato de Licencia de Software de Usuario Final (“EULA”).
     * @license Incluida licencia carpeta de Informacion
     * @version 1.0
     *
     * Este contrato de licencia de software de usuario final (EULA, por sus siglas en inglés)
     * es un acuerdo vinculante entre el usuario titular de la licencia (“Usuario final”) y VoIP Technology S.A.,
     * que expone los términos y condiciones que rigen el uso y el funcionamiento de los productos
     * de software de computadoras propiedad de CallOne Contact Center (el “Software”) y las especificaciones técnicas
     * escritas para el uso y el funcionamiento del Software (la “Documentación”). Donde el sentido
     * y el contexto lo permitan, las referencias en este EULA al Software incluyen la Documentación.
     * Al descargar e instalar, copiar o, en otras palabras, usar el Software y/o aceptar este EULA,
     * el Usuario final acuerda reconocer como vinculante los términos y condiciones de este EULA.
     *
     * Si el Usuario final no acuerda ni acepta los términos de este EULA, es posible que el Usuario
     * final no tenga acceso ni pueda usar el Software.
     */
    
    use App\Post;
    use App\Category;
    use App\Tag;
    use App\Photo;
    use Carbon\Carbon;
    use Validator;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Http\Requests\StorePostRequest;
    use Illuminate\Support\Facades\Storage;


    class PostsController extends Controller
    {
        /**
         * Public function
         * Index()
         * 
         * Listado de todos los post
         * @return {JSON} Arreglo de post y envio a vista
         */
        public function index()
        {
            $posts = Post::all();
            return view('admin.posts.index', compact('posts'));
        }

        /**
         * Public function
         * edit(Post $post)
         * 
         * Pantalla de edición de post
         * @param {Integer} $post IdPost
         * @return {HTML} vista de edición
         */
        public function edit(Post $post)
        {
            $categories = Category::all();
            $tags = Tag::all();
            return view('admin.posts.edit', compact('post', 'categories', 'tags'));
        }

        /**
         * Public function
         * store(Request $request)
         * 
         * Crea el titulo del nuevo Post y envia la url de la vista
         * @param  {Array} $request Datos del formulario
         * @return {String} Url del la vista de editar
         */
        public function store(Request $request)
        {
            if ( ($this->validateFormTitlePost( $request->all()))->fails() ) {
                echo 'Error';
            }
            else {
                $post = Post::create( $request->only('title') );
                echo route('admin.posts.edit', $post);
            }
        }

        /**
         * public function
         * validateFormTitlePost($postCreate)
         * 
         * Valida los datos del formulario
         * @param  {Integer} $postCreate identificado del nuevo post
         * @return {Boolean} falso o verdadero dependiendo del resultado de la validación
         */
        public function validateFormTitlePost($postCreate)
        {
            $rules = array(
                'title' => 'required|min:3|unique:posts'
            );
            return $validator = Validator::make($postCreate, $rules);
        }

        /**
         * public function
         * update(Post $post, StorePostRequest $request)
         * 
         * Actualiza los datos del post ingresado
         * @param  {Integer} $post Identificador del post
         * @param  {Array}   $request arreglo de los datos del post con formato
         * @return {String}  mensaje de que la publicación fue guardada exitosamente
         */
        public function update(Post $post, StorePostRequest $request)
        {
            $post->update($request->all());
            $post->syncTags($request->get('tags'));
            // Retorna la misma vista con un mensaje
            return redirect()->route('admin.posts.edit', $post)->with('flash', 'Tu publicación ha sido guardada');
        }

        /**
         * public function
         * destroy(Post $post)
         * 
         * Destruye el post y sus elementos
         * @param  {Integer} $post identificador del post
         * @return {Null}
         */
        public function destroy(Post $post)
        {
            $post->tags()->detach(); // Quita las relaciones de este post con estas etiquetas
            $post->photos->each(function($photo){
                $photoPath = str_replace('storage', 'public', $photo->url);
                Storage::delete($photoPath);
                $photo->delete(); // Elimina la relacion con la base de datos
            });
            $post->delete();
        }

        /**
         * public function
         * loadJSONData(Request $request)
         * 
         * API REST de los datos de los post
         * @param  {Array} $request de datos de la consulta
         * @return {JSON} resultados de la consulta
         */
        public function loadJSONData(Request $request)
        {
            $orderby = $request->input('order.0.column');
            $sort['col'] = $request->input('columns.' . $orderby . '.data');    
            $sort['dir'] = $request->input('order.0.dir');

            $query = Post::where('title', 'like', '%'. $request->input('search.value') .'% ')
                ->orWhere('excerpt', 'like', '%'. $request->input('search.value') .'%');

            $output['recordsTotal'] = $query->count();

            $output['data'] = $query
                    ->orderBy($sort['col'], $sort['dir'])
                    ->skip($request->input('start'))
                    ->take($request->input('length',10))
                    ->get();

            $output['recordsFiltered'] = $output['recordsTotal'];

            $output['draw'] = intval($request->input('draw'));

            return $output;
        }

    }
