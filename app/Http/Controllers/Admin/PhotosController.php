<?php

    namespace App\Http\Controllers\Admin;

    /**
     * VoIP Technology S.A.
     * Control De Blog.
     *
     * @copyright Copyright (c) 2018, VoIP Technology S.A.
     * @link   http://voiptech.com.mx
     * @author Ramses Aguirre Farrera <ramsesaguirre2012@gmail.com>
     * @author Contacto <contacto#voiptech.com.mx>
     * @license Contrato de Licencia de Software de Usuario Final (“EULA”).
     * @license Incluida licencia carpeta de Informacion
     * @version 1.0
     *
     * Este contrato de licencia de software de usuario final (EULA, por sus siglas en inglés)
     * es un acuerdo vinculante entre el usuario titular de la licencia (“Usuario final”) y VoIP Technology S.A.,
     * que expone los términos y condiciones que rigen el uso y el funcionamiento de los productos
     * de software de computadoras propiedad de CallOne Contact Center (el “Software”) y las especificaciones técnicas
     * escritas para el uso y el funcionamiento del Software (la “Documentación”). Donde el sentido
     * y el contexto lo permitan, las referencias en este EULA al Software incluyen la Documentación.
     * Al descargar e instalar, copiar o, en otras palabras, usar el Software y/o aceptar este EULA,
     * el Usuario final acuerda reconocer como vinculante los términos y condiciones de este EULA.
     *
     * Si el Usuario final no acuerda ni acepta los términos de este EULA, es posible que el Usuario
     * final no tenga acceso ni pueda usar el Software.
     */
    

    use App\Photo;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Storage;

    class PhotosController extends Controller
    {
        /**
         * public function
         * store()
         * 
         * Almacena el nombre de la imagen en la base de datos y en el directorio public
         * @return {Null}
         */
        public function store()
        {
            $post_id = (int) request()->get('post');
            $this->validate(request(), [
                'photo' => 'image|max:2048'
            ]);

            $photo = request()->file('photo');

            Photo::create([
                'url' => Storage::url($photo->store('public')),
                'post_id' => $post_id
            ]);
        }

        /**
         * public function
         * destroy(Photo $photo)
         * 
         * Elimina la imagen dentro del directorio publico
         * @param  {Path} $photo direccion fisica de la imagen
         * @return {Null}
         */
        public function destroy(Photo $photo)
        {
            $photoPath = str_replace('storage', 'public', $photo->url);
            Photo::find($photo->id)->delete();
            Storage::delete($photoPath);
        }
    }
