<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /* Considera en la captura masiva solo estos campos */
    protected $fillable = [
        'title', 'body', 'iframe', 'excerpt', 'published_at', 'category_id'
    ];
    /* permite en formato fecha al campo published_at */
    protected $dates = ['published_at'];
    
    public function getRouteKeyName()
    {
        return 'url';
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function scopePublished($query)
    {
        $query->whereNotNull('published_at')
                ->where('published_at', '<=', Carbon::now())
                ->latest('published_at');
    }

    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;
        $url = str_slug($title);
        /* $duplicateUrlCount = (Post::where('url', 'LIKE', "{$url}%"))->count();
        if($duplicateUrlCount)
        {
            $url .= '-' . ++$duplicateUrlCount;
        } */
        $this->attributes['url'] = $url;
    } 

    public function setPublishedAtAttribute($published_at)
    {
        $this->attributes['published_at'] = $published_at != null ? Carbon::parse($published_at) : null;
    }

    public function setCategoryIdAttribute($category)
    {
        $this->attributes['category_id'] = Category::find($category) ? $category : Category::create(['name' => $category])->id;
    }

    public function syncTags($tags)
    {
        $tagIds = collect($tags)->map(function($tag){
            return Tag::find($tag) ? $tag : (string) Tag::create(['name' => $tag])->id;
        });

        // Guarda las etiquetas
        $this->tags()->sync($tagIds);
    }
}
