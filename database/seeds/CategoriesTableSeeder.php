<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();

        $category = new Category;
        $category->name = 'Desarrollo';
        $category->save();

        $category = new Category;
        $category->name = 'Tecnologia';
        $category->save();
    }
}
