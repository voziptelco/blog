<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::truncate();

        $tag = new Tag;
        $tag->name = 'GNU/Linux';
        $tag->save();

        $tag = new Tag;
        $tag->name = 'Developer PHP';
        $tag->save();
    }
}
