<?php

use App\Post;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::deleteDirectory('public');
        Post::truncate();

        $post = new Post;
        $post->title = "Mi primero post";
        $post->url = str_slug("Mi primero post");
        $post->excerpt = "contenido de mi primer post";
        $post->body = "<p>Contenido largo de mi primer post realizado en Laravel</p>";
        $post->published_at = Carbon::now()->subDays(4);
        $post->category_id = 1;
        $post->save();

        $post = new Post;
        $post->title = "Mi segundo post";
        $post->url = str_slug("Mi segundo post");
        $post->excerpt = "Extrato de mi segundo post";
        $post->body = "<p>Contenido de mi segundo post </p>";
        $post->published_at = Carbon::now()->subDays(3);
        $post->category_id = 2;
        $post->save();

        $post = new Post;
        $post->title = "Mi tercer post";
        $post->url = str_slug("Mi tercer post");
        $post->excerpt = "Extrato de mi tercer post";
        $post->body = "<p>Contenido de mi tercer post </p>";
        $post->published_at = Carbon::now()->subDays(2);
        $post->category_id = 2;
        $post->save();
    }
}
