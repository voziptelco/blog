<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $user = new User;
        $user->name = "Ramsés Aguirre";
        $user->email = "ramsesaguirre2012@gmail.com";
        $user->password = bcrypt('123');
        $user->save();
    }
}
