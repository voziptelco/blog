@extends('layout')

@section('meta-title', $post->title)

@section('meta-description', $post->excerpt)

@section('content')
<article class="post container">
    @if ($post->photos->count() === 1)
        <figure><img src="{{ $post->photos->first()->url }}" alt="" class="img-responsive"></figure>
    @elseif($post->photos->count() > 1)
      @include('posts.carousel')
    @elseif ($post->iframe)
        <div class="iframe-wrapper">
            {!! $post->iframe !!}
        </div>
    @endif
    <div class="content-post">
      <header class="container-flex space-between">
        <div class="date">
          <span class="c-gris">{{ $post->published_at->format('M d') }}</span>
        </div>
        <div class="post-category">
        <span class="category text-capitalize"><a href="{{ route('categories.show', $post->category)}}">{{ $post->category->name }}</a></span>
        </div>
      </header>
      <h1>{{ $post->title }}</h1>
        <div class="divider"></div>
        <div class="image-w-text">
          {!! $post->body !!}
        </div>

        <footer class="container-flex space-between">
          
        @include('partials.social-link', ['description' => $post->title])

          <div class="tags container-flex">
                @foreach($post->tags as $tag)
                  <span class="tag c-gray-1 text-capitalize"><a href="{{ route('tags.show', $tag) }}">#{{ $tag->name }}</a></span>
                @endforeach
          </div>
        </footer>
        <div class="comments">
        <div class="divider"></div>
        <div id="disqus_thread"></div>
        
        @include('partials.disqus-script')

      </div><!-- .comments -->
    </div>
  </article>
@stop

@push('styles')
<link rel="stylesheet" href="/css/bootstrap.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css">
<link rel="stylesheet" href="/css/docco.css">
@endpush

@push('scripts')
<script id="dsq-count-scr" src="//zendero.disqus.com/count.js" async></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
<script src="/js/bootstrap.js"></script>
<script>
$(document).ready(function() {
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
});
</script>
@endpush