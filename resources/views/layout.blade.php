<!DOCTYPE html>
<html lang="es-MX" prefix="og: http://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>@yield('meta-title', ' Principal | VoIP Technology Blog')</title>
	<meta name="description" content="@yield('meta-description', 'Este es el blog de Ramses Aguirre')">
	<!-- This site is optimized with the Yoast SEO plugin v6.2 - https://yoa.st/1yg?utm_content=6.2 -->
<!-- <link rel="canonical" href="http://noticiasglobal.com.mx/2018/04/06/magistrada-magistrados-del-tribunal-electoral-del-guerrero-juzgan-sin-perspectiva-genero-violentan-los-derechos-politicos-las-mujeres-acusan-integrantes-la-red/" />
<meta property="og:locale" content="es_MX" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Magistrada y magistrados del Tribunal Electoral del Estado de Guerrero, juzgan sin perspectiva de género y violentan los derechos políticos de las mujeres, acusan integrantes de la Red. - Agencia Global" />
<meta property="og:description" content="Interpondrán mujeres recurso legal ante TRIFE por violencia política en razón de género. El resolutivo del TEEG califica de &#8220;Frívolo&#8221; el recurso de impugnación de Zulma Carbajal contra la dirigencia estatal del PRD.   Agencia Global. Chilpancingo, Guerrero, 5 de abril de 2018.- Integrantes de la Red para el Avance Político de las Mujeres Guerrerenses,..." />
<meta property="og:url" content="http://noticiasglobal.com.mx/2018/04/06/magistrada-magistrados-del-tribunal-electoral-del-guerrero-juzgan-sin-perspectiva-genero-violentan-los-derechos-politicos-las-mujeres-acusan-integrantes-la-red/" />
<meta property="og:site_name" content="Agencia Global" />
<meta property="article:publisher" content="https://www.facebook.com/periodicoglobal.mx" />
<meta property="article:tag" content="Agencia Global Noticias Guerrero" />
<meta property="article:tag" content="guerrero" />
<meta property="article:tag" content="Noticias Guerrero" />
<meta property="article:tag" content="Red para el avance politico de mujeres" />
<meta property="article:tag" content="Tribunal Electoral del Estado de Guerrero" />
<meta property="article:tag" content="Violencia Política" />
<meta property="article:section" content="Ciudadanía" />
<meta property="article:published_time" content="2018-04-06T08:06:01+00:00" />
<meta property="og:image" content="http://noticiasglobal.com.mx/wp-content/uploads/2018/04/red-1.jpg" />
<meta property="og:image:width" content="1417" />
<meta property="og:image:height" content="1063" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Interpondrán mujeres recurso legal ante TRIFE por violencia política en razón de género. El resolutivo del TEEG califica de &#8220;Frívolo&#8221; el recurso de impugnación de Zulma Carbajal contra la dirigencia estatal del PRD.   Agencia Global. Chilpancingo, Guerrero, 5 de abril de 2018.- Integrantes de la Red para el Avance Político de las Mujeres Guerrerenses,..." />
<meta name="twitter:title" content="Magistrada y magistrados del Tribunal Electoral del Estado de Guerrero, juzgan sin perspectiva de género y violentan los derechos políticos de las mujeres, acusan integrantes de la Red. - Agencia Global" />
<meta name="twitter:image" content="http://noticiasglobal.com.mx/wp-content/uploads/2018/04/red-1.jpg" />
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"http:\/\/noticiasglobal.com.mx\/","name":"Agencia Global Guerrero","potentialAction":{"@type":"SearchAction","target":"http:\/\/noticiasglobal.com.mx\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"Organization","url":"http:\/\/noticiasglobal.com.mx\/2018\/04\/06\/magistrada-magistrados-del-tribunal-electoral-del-guerrero-juzgan-sin-perspectiva-genero-violentan-los-derechos-politicos-las-mujeres-acusan-integrantes-la-red\/","sameAs":["https:\/\/www.facebook.com\/periodicoglobal.mx"],"@id":"#organization","name":"Noticias Global Guerrero","logo":"http:\/\/noticiasglobal.com.mx\/wp-content\/uploads\/2017\/10\/LOGO-PARA-FACEBOOK-PERFIL.jpg"}</script> -->
<!-- / Yoast SEO plugin. -->

	<link rel="stylesheet" href="/css/normalize.css">
	<link rel="stylesheet" href="/css/framework.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/responsive.css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

	@stack('styles')
</head>
<body>
	<div class="preload"></div>
	<header class="space-inter">
		<div class="container container-flex space-between">
			<figure class="logo"><img src="/img/Blog_pic.png" alt=""></figure>
			<nav class="custom-wrapper" id="menu">
				<div class="pure-menu"></div>
				<ul class="container-flex list-unstyled">
					<li><a href="/" class="text-uppercase">Inicio</a></li>
					<li><a href="about.html" class="text-uppercase">Acerca De</a></li>
					<li><a href="archive.html" class="text-uppercase">Archivos</a></li>
					<li><a href="contact.html" class="text-uppercase">Contacto</a></li>
				</ul>
			</nav>
		</div>
    </header>
    
    <!- Contenido -->
   @yield('content') 

	<section class="footer">
		<footer>
			<div class="container">
				<figure class="logo"><img src="/img/logo.png" alt=""></figure>
				<nav>
					<ul class="container-flex space-center list-unstyled">
						<li><a href="index.html" class="text-uppercase c-white">home</a></li>
						<li><a href="about.html" class="text-uppercase c-white">about</a></li>
						<li><a href="archive.html" class="text-uppercase c-white">archive</a></li>
						<li><a href="contact.html" class="text-uppercase c-white">contact</a></li>
					</ul>
				</nav>
				<div class="divider-2"></div>
				<p>Nunc placerat dolor at lectus hendrerit dignissim. Ut tortor sem, consectetur nec hendrerit ut, ullamcorper ac odio. Donec viverra ligula at quam tincidunt imperdiet. Nulla mattis tincidunt auctor.</p>
				<div class="divider-2" style="width: 80%;"></div>
				<p>© 2017 - VoIP Technology S.A. All Rights Reserved. Designed & Developed by <span class="c-white">Agencia De La Web</span></p>
				<ul class="social-media-footer list-unstyled">
					<li><a href="#" class="fb"></a></li>
					<li><a href="#" class="tw"></a></li>
					<li><a href="#" class="in"></a></li>
					<li><a href="#" class="pn"></a></li>
				</ul>
			</div>
		</footer>
	</section>
	@stack('scripts')
</body>
</html>