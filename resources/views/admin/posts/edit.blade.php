@extends('admin.layout')

@section('header')
    <h1>
        POSTS
        <small>Crear publicación</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Crear</li>
      </ol>
@stop

@section('content')
<div class="row">
    <form action="{{ route('admin.posts.update', $post) }}" method="POST">
        @method('PUT')
        @csrf
    <div class="col-md-8">
        <div class="box box-primary">
            
                <div class="box-body">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">Título de la publicación</label>
                        <input name="title" placeholder="Ingrese aquí el titulo de la publicación" type="text" class="form-control" value="{{ old('title', $post->title) }}" autofocus>
                        {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                        <label for="body">Contenido de la publicación</label>
                        <textarea name="body" id="editor" class="form-control" rows="10" placeholder="Ingrese aquí el contenido completo de la publicación">{{ old('body', $post->body ) }}</textarea>
                        {!! $errors->first('body', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('iframe') ? 'has-error' : '' }}">
                        <label for="iframe">Contenido embebido (iframe)</label>
                        <textarea name="iframe" class="form-control" rows="2" placeholder="Ingrese aquí el contenido embebido (Iframe) de audio o video">{{ old('iframe', $post->iframe ) }}</textarea>
                        {!! $errors->first('iframe', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="row">
                        @foreach ($post->photos as $photo)
                            <div class="col-md-2">
                                <button type="button" onclick="javascript: eliminarImagen({{ $photo->id }}, '{{ route('admin.photos.destroy', $photo) }}', $(this).parent())" class="btn btn-danger btn-xs" style="position: absolute">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <img src="{{ url($photo->url) }}" alt="" class="img-responsive">
                            </div>
                        @endforeach
                    </div>
                </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <label>Fecha de publicación:</label>

                    <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="published_at" class="form-control pull-right" id="datepicker" value="{{ old('published_at', Carbon\Carbon::parse($post->published_at)->format('m/d/Y')) }}">
                    </div>
                </div>
                <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                    <label for="category_id">Categorías</label>
                    <select name="category_id" id="" class="form-control select2">
                        <option value="">Selecciona una categoría</option>
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}"
                            {{ old('category_id', $post->category_id ) == $category->id ? 'selected' : '' }}
                            >{{ $category->name }}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('category_id', '<span class="help-block">:message</span>') !!}
                </div>
                <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                    <label for="tags">Etiquetas</label>
                    <select name="tags[]" class="form-control select2" multiple="multiple" data-placeholder="Selecciona una o más etiquetas" style="width: 100%;">
                        @foreach($tags as $tag)
                            <option {{ collect(old('tags', $post->tags->pluck('id')))->contains($tag->id) ? 'selected' : '' }} value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('tags', '<span class="help-block">:message</span>') !!}
                </div>
                <div class="form-group {{ $errors->has('excerpt') ? 'has-error' : '' }}">
                    <label for="excerpt">Extracto de la publicación</label>
                    <textarea name="excerpt" class="form-control" rows="3" placeholder="Ingrese aquí un extracto de la publicación">{{ old('excerpt', $post->excerpt) }}</textarea>
                    {!! $errors->first('excerpt', '<span class="help-block">:message</span>') !!}
                </div>
                <div class="form-group">
                    <div class="dropzone"></div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Guardar Publicación</button>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
@stop

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="/adminlte/bower_components/select2/dist/css/select2.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js"></script>
<!-- Select2 -->
<script src="/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- CK Editor -->
<script src="/adminlte/bower_components/ckeditor/ckeditor.js"></script>
<!-- bootstrap datepicker -->
<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script>  
//Date picker
$('#datepicker').datepicker({
    autoclose: true
});

$('.select2').select2({
    tags: true
});

CKEDITOR.replace('editor');
/* CKEDITOR.config.height = 260;*/

var myDropzone = new Dropzone('.dropzone', {
    url: '/admin/posts/{{ $post->id }}/photos',
    acceptedFiles: 'image/*',
    maxFilesize: 2,
    paramName: 'photo',
    params: {
            post:'{{ $post->id }}'
    },
    headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
    },
    dictDefaultMessage: 'Arrastra la foto(s) aquí para subirla(s)'
});

myDropzone.on('error', function(file, res){
    var msg = res.errors.photo[0];
    $('.dz-error-message:last > span').text(msg);
});

Dropzone.autoDiscover = false;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
    }
});


function eliminarImagen(id, ruta, elemento){
    $( elemento ).remove();
    $.ajax({
        type:'POST',
        url: ruta,
        data:{
            _method: 'delete', 
            photo_id: id
        },
        success:function(data){
            $( elemento ).remove();
            console.log(data);
        }
    });
}
</script>
@endpush
