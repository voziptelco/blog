<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<form id="formIngresarPost" action="{{ route('admin.posts.store') }}" method="POST">
        @csrf
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar el título de la publicación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="GroupIngresaTitulo" class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
        <!-- <label for="title">Titulo de la publicación</label> -->
        <input name="title" id="title" placeholder="Ingrese aquí el titulo de la publicación" type="text" class="form-control" value="{{ old('title') }}" autofocus required>
        {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button class="btn btn-primary">Crear Publicación</button>
      </div>
    </div>
  </div>      
  </form>
</div>

<script>
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
    }
  });
  
  $(document).ready(function() {
    $('#formIngresarPost').submit(function(event){
      $('#Error').remove();
      /* Datos de envio de la forma modal */
      var formData = {
            'title'              : $('input[name=title]').val()
        };
      /* Procesamiento AJAX */
      $.ajax({
            type : 'POST',
            url  : "{{ route('admin.posts.store') }}",
            data : formData,
            dataType : 'text'
        })
          .done(function(data) {
            if(data === 'Error'){
              $('#GroupIngresaTitulo').addClass('has-error');
              $('#title').focus();
              $('#GroupIngresaTitulo').append('<span id="Error" class="help-block">Publicación, ya existe!</span>');
            }
            else{
              window.location.href = data;
            }
          });
      event.preventDefault();
    });
  });
</script> 