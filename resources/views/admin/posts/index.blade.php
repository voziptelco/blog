@extends('admin.layout')

@section('header')
    <h1>
        POSTS
        <small>Listado de todas las publicaciones</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Posts</li>
      </ol>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Listado de publicaciones</h3>
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#exampleModal">
        <i class="fa fa-plus"></i>&nbsp;
        Crear publicación</button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="posts-table" class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>ID</th>
            <th>Título</th>
            <th>Extracto</th>
            <th>Fecha Publicación</th>
            <th style="text-align:center;width:100px;">
                Acciones 
                <!-- <button type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-success btn-xs dt-add">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</button> -->
            </th>
        </tr>
        </thead>
        <tbody>
            <!-- @foreach($posts as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->excerpt }}</td>
                    <td>
                    @if (!(empty($post->excerpt)) OR !(empty($post->body)) OR !(empty($post->published_at)))
                        <a href="{{ route('posts.show', $post) }}" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                    @else
                    <a href="{{ route('posts.show', $post) }}" target="_blank" class="btn disabled btn-xs btn-default"><i class="fa fa-eye"></i></a>
                    @endif
                        <a href="{{ route('admin.posts.edit', $post) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                        <button type="button" class="btn btn-xs btn-danger" onclick="javascript: eliminarPost({{  $post->id }}, '{{ route('admin.posts.destroy', $post) }}', $(this).parent().parent())"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            @endforeach     -->
        </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
@stop

@push('styles')
<!-- DataTables -->
<link rel="stylesheet" href="/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endpush

@push('scripts')
<!-- Sweatalert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- DataTables -->
<script src="/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
/* Carga la tabla de datos */
$(function () {
    otable = $('#posts-table').DataTable({
        'paging': true,
        "ajax": "{{ route('admin.posts.loadJSONData') }}",
        "processing":true,
        "serverSide":true,
        "stateSave":true,
        "columns": [
                { "data": "id", "class":"small", "sortable": false },
                { "data": "title", "class":"small", "width": "15%"  },
                { "data": "excerpt", "class":"small", "width": "55%"  },
                { "data": "published_at", "class":"small"  },
                {
                    "data": null,
                    "sortable": false,
                    "class":"text-center",
                    "render": function (data) {
                        return '<a href="/blog/'+ data.url + '" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a> &nbsp;' +
                        '<a href="posts/' + data.url + '" class="btn btn-xs btn-info small"><i class="fa fa-pencil"></i></a> &nbsp;' +
                        '<button type="button" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></button>';
                    }
                }
            ],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta tabla",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    })
    
})

/* Activa el boton eliminar post */
$('#posts-table tbody').on( 'click', 'button', function () {
    var data = otable.row( $(this).parents('tr') ).data();
    eliminarPost( data.id, 'posts/' + data.url, $(this).parent().parent());
} );


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
    }
});

/* Funcion ajax eliminar */
function eliminarPost(id, ruta, elemento){
    swal({
        title: "¿Desea eliminar este post?",
        text: "Esta realmente seguro que desea eliminar el post!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Cancelar", "Si, deseo eliminar!"],
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type:'POST',
                url: ruta,
                data:{
                    _method: 'delete', 
                    post_id: id
                },
                success:function(data){
                    $( elemento ).remove();
                    console.log(data);
                    swal("El post ha sido eliminado del sistema!", {
                        icon: "success",
                    });
                }
            });
        } 
    });    

}
</script>

@endpush